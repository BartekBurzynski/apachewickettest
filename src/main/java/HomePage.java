import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.form.Form;

import java.util.Arrays;
import java.util.List;

public class HomePage extends WebPage {
    private static final long serialVersionUID = 1L;

    Form form;

    public HomePage(final PageParameters parameters) {
        super(parameters);

        List<String> persons = Arrays.asList("user", "official");
        form = new Form("homeForm");
        form.add(new DropDownChoice<String>("person", new Model(), persons));
    }
}