package com.crudcompany;

public class User extends Person {
   String city;

    public User(String name, String lastName, String city) {
        super(name, lastName);
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
