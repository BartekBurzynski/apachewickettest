package com.crudcompany;

public class Official extends Person {
    String position;

    public Official(String name, String lastName, String position) {
        super(name, lastName);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
