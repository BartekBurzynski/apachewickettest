//package com.mycompany;
//
//import org.apache.wicket.Session;
//import org.apache.wicket.ajax.AjaxRequestTarget;
//import org.apache.wicket.ajax.markup.html.form.AjaxButton;
//import org.apache.wicket.markup.html.WebMarkupContainer;
//import org.apache.wicket.markup.html.WebPage;
//import org.apache.wicket.markup.html.basic.Label;
//import org.apache.wicket.markup.html.form.Form;
//import org.apache.wicket.markup.html.form.TextField;
//import org.apache.wicket.markup.html.list.ListItem;
//import org.apache.wicket.markup.html.list.ListView;
//import org.apache.wicket.markup.html.panel.FeedbackPanel;
//import org.apache.wicket.model.Model;
//import org.apache.wicket.model.PropertyModel;
//import org.apache.wicket.model.util.ListModel;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class FormPage extends WebPage {
//    private static final long serialVersionUID = 1L;
//    private final TextField<String> nameTextField;
//    private final TextField<String> lastNameTextField;
//    private final TextField<String> ageTextField;
//
//    private final FeedbackPanel feedback;
//
//    private final WebMarkupContainer peopleContainer;
//    private final ListView<Person> listView;
//
//    public FormPage() {
//        List<Person> people = new ArrayList<>();
//
//
//        feedback = new FeedbackPanel("feedback");
//        feedback.setOutputMarkupId(true);
//        add(feedback);
//
//        Form form = new Form("form");
//
//        form.add(new Label("formName","Podaj imię"));
//        form.add(new Label("formLastName","Podaj nazwisko"));
//        form.add(new Label("formAge","Podaj wiek"));
//
//        Model<String> nameTextValueModel = new Model<>("");
//        nameTextField = new TextField<>("nameText", nameTextValueModel);
//        form.add(nameTextField);
//
//        Model<String> lastNameTextValueModel = new Model<>("");
//        lastNameTextField = new TextField<>("lastNameText", lastNameTextValueModel);
//        form.add(lastNameTextField);
//
//        Model<String> ageTextValueModel = new Model<>("");
//        ageTextField = new TextField<String>("ageText", ageTextValueModel);
//        form.add(ageTextField);
//
//        form.add(new AjaxButton("submit") {
//
//            @Override
//            protected void onSubmit(AjaxRequestTarget target) {
//                String nameValue = nameTextValueModel.getObject();
//                String lastNameValue = lastNameTextValueModel.getObject()
//                String ageValue = ageTextValueModel.getObject();
//                Session.get().error("WARTOSC2: " + nameValue);
//                people.add(new Person(nameValue, lastNameValue, ageValue, nameValue ));
//                target.add(feedback);
//                target.add(peopleContainer);
//
//            }
//        });
//        add(form);
//
//        ListModel<Person> peopleModel = new ListModel<>(people);
//
//        //    people.add(new Person("Jan","Kowalski","22"));
//        //    people.add(new Person("Tomek","csasd","45"));
//
//        peopleContainer = new WebMarkupContainer("peopleContainer");
//        peopleContainer.setOutputMarkupId(true);
//
//        listView = new ListView<Person>("people", peopleModel) {
//
//            @Override
//            protected void populateItem(ListItem<Person> item) {
//                item.add(new Label("name", item.getModelObject().getName()));
//                item.add(new Label("lastName", item.getModelObject().getLastName()));
//                item.add(new Label("age", item.getModelObject().getCity()));
//                item.add(new Label("age", item.getModelObject().getCity()));)
//            };
//        listView.setOutputMarkupId(true);
//        peopleContainer.add(listView);
//            add(peopleContainer);
//        }
//
//
//    }