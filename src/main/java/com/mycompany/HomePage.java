package com.mycompany;


import org.apache.wicket.Session;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	public static final String USER = "user";
	public static final String OFFICIAL = "official";
	private final TextField<String> nameTextField;
	private final TextField<String> lastNameTextField;
	private final TextField<String> cityTextField;
	private final TextField<String> positionTextField;
	private CheckBox checkBoxField;

	private final FeedbackPanel feedback;

	private final WebMarkupContainer peopleContainer;
	private final ListView<Person> listView;
	private final Label stateDescription;

	List<Person> people = new ArrayList<>();

	private Person personEdit = null;
	private FormState formState = FormState.ADD;

	Form form;
	private final DropDownChoice<String> personTypeDropDown;
	private final Model<String> statDModel;

	public HomePage(final PageParameters parameters) {
		super(parameters);


		List<Person> people = new ArrayList<>();


		feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		add(feedback);


		List<String> persons = Arrays.asList(USER, OFFICIAL);
		form = new Form("homeForm");

//		form.add(stateDescription = new Label("stateDescription", new IModel<String>() {
//
//			@Override
//			public String getObject() {
//				return formState == FormState.ADD ? "DODAJESZ UZYTKOWNIKA" : "EDYTUJESZ";
//			}
//		}));

		statDModel = new Model<>(getDescription(formState));
		form.add(stateDescription = new Label("stateDescription", statDModel));

		stateDescription.setOutputMarkupId(true);

		Model<String> personTypeModel = new Model<>(USER);
		personTypeDropDown = new DropDownChoice<>("person", personTypeModel, persons);
		personTypeDropDown.add(new OnChangeAjaxBehavior() {
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				target.add(form);
			}
		});
		form.add(personTypeDropDown);

		form.add(new Label("homeFormName","Podaj imię"));
		form.add(new Label("homeFormLastName","Podaj nazwisko"));
		form.add(new Label("homeFormCity","Podaj miasto"));
		form.add(new Label("homeFormPosition","Podaj stanowisko"));
	//	form.add(new CheckBox("checkActive"){
	//		@Override
	//		public boolean isVisible() {
	//			return Objects.equals(personTypeModel.getObject(), USER);
	//		}
	//	});
	//	add(form);


		Model checkBoxValueModel = new Model<>(false);
		checkBoxField = new CheckBox("checkActive",checkBoxValueModel){
			@Override
			public boolean isVisible() {
				return Objects.equals(personTypeModel.getObject(), OFFICIAL);
			}
		};




		checkBoxField.checkRequired();
		form.add(checkBoxField);

		Model<String> nameTextValueModel = new Model<>("");
		nameTextField = new TextField<>("nameText", nameTextValueModel);
		nameTextField.setLabel(Model.of("Imię"));
		nameTextField.add(EmailAddressValidator.getInstance());
		nameTextField.setRequired(true);
		form.add(nameTextField);

		Model<String> lastNameTextValueModel = new Model<>("");
		lastNameTextField = new TextField<>("lastNameText", lastNameTextValueModel);
		lastNameTextField.setRequired(true);
		lastNameTextField.setLabel(Model.of("Nazwisko"));
		lastNameTextField.add(StringValidator.lengthBetween(2,40));
		form.add(lastNameTextField);

		Model<String> cityTextValueModel = new Model<>("");
		cityTextField = new TextField<String>("cityText", cityTextValueModel) {
			@Override
			public boolean isVisible() {
				return Objects.equals(personTypeModel.getObject(), USER);
			}
		};
		cityTextField.setLabel(Model.of("Miasto"));
		cityTextField.setRequired(true);
		form.add(cityTextField);

		Model<String> positionTextValueModel = new Model<>("");
		positionTextField = new TextField<String>("positionText", positionTextValueModel){
			@Override
			public boolean isVisible() {
				return  Objects.equals(personTypeModel.getObject(),OFFICIAL);
			}
		};
		positionTextField.setLabel(Model.of("stanowisko"));
		positionTextField.setRequired(true);
		form.add(positionTextField);

		form.add(new AjaxButton("test") {

	//		@Override
	//		protected void onSubmit(AjaxRequestTarget target) {
	//			super.onSubmit(target);
	//			String nameValue = nameTextValueModel.getObject();
	//			String lastNameValue = lastNameTextValueModel.getObject();
	//			String cityValue = cityTextValueModel.getObject();
	//			String positionValue = positionTextValueModel.getObject();
	//			String test = "dupaDupa";
	//			while (lastNameValue.equals("dupa")){
	//				people.add(new Person(nameValue ,lastNameValue + test, cityValue, positionValue));
//
//				}
//
//				target.add(peopleContainer);
//			}
		});

		form.add(new AjaxButton("submit") {


			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				String nameValue = nameTextValueModel.getObject();
				String lastNameValue = lastNameTextValueModel.getObject();
				String cityValue = cityTextValueModel.getObject();
				String positionValue = positionTextValueModel.getObject();
				if (formState == FormState.ADD) {
					people.add(new Person(nameValue, lastNameValue, cityValue, positionValue));
					Session.get().info("DODAŁES USERA");
				} else {
					if (personEdit == null ) {
						throw new RuntimeException();
					}
					personEdit.setName(nameValue);
					personEdit.setPosition(positionValue);
					personEdit.setLastName(lastNameValue);
					personEdit.setCity(cityValue);
					formState = FormState.ADD;
					statDModel.setObject(getDescription(formState));
					personEdit = null;
					Session.get().info("EDYCJA POSZLA OK");
				}

				clearFormModel();
				target.add(form);
				target.add(feedback);
				target.add(peopleContainer);

			}

			private void clearFormModel() {
				nameTextValueModel.setObject("");
				lastNameTextValueModel.setObject("");
				cityTextValueModel.setObject("");
				positionTextValueModel.setObject("");

			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				target.add(feedback);
			}
		});
		add(form);


		ListModel<Person> peopleModel = new ListModel<>(people);

		peopleContainer = new WebMarkupContainer("peopleContainer");
		peopleContainer.setOutputMarkupId(true);

		Form<Object> tableForm = new Form<>("tableForm");

		listView = new ListView<Person>("people", peopleModel) {

			@Override
			protected void populateItem(ListItem<Person> item) {
				final Person dto = item.getModelObject();
				item.add(new Label("name", dto.getName()));
				item.add(new Label("lastName", dto.getLastName()));
				item.add(new Label("city", dto.getCity()));
				item.add(new Label("position", dto.getPosition()));
				item.add(new AjaxButton("edit") {
					@Override
					protected void onSubmit(AjaxRequestTarget target) {
						super.onSubmit(target);


						formState = FormState.EDIT;
						statDModel.setObject(getDescription(formState));
						personEdit = dto;
						nameTextValueModel.setObject(dto.getName());
						cityTextValueModel.setObject(dto.getCity());
						positionTextValueModel.setObject(dto.getPosition());
						lastNameTextValueModel.setObject(dto.getLastName());
						target.add(form);
						target.add(stateDescription);

					}
				});
				item.add(new AjaxButton("delete") {
					@Override
					protected void onSubmit(AjaxRequestTarget target) {
						super.onSubmit(target);
						people.remove(dto);
						target.add(peopleContainer);
					}
				});
			}
		};
		listView.setOutputMarkupId(true);

		tableForm.add(listView);
		peopleContainer.add(tableForm);
		add(peopleContainer);
	}

	private String getDescription(FormState formState) {
		return formState == FormState.ADD ? "ADD" : "EDIT";
	}
}








//import org.apache.wicket.markup.html.WebPage;
//import org.apache.wicket.markup.html.basic.Label;
//import org.apache.wicket.markup.html.link.BookmarkablePageLink;
//import org.apache.wicket.model.Model;
//import org.apache.wicket.request.mapper.parameter.PageParameters;
//
//public class HomePage extends WebPage {
//	private static final long serialVersionUID = 1L;
//	private int i;
//
//	public HomePage(final PageParameters parameters) {
//		super(parameters);
//
//		add(new Label("version", "ddd: " + getApplication().getFrameworkSettings().getVersion()));
//
//		Model<String> labelModel = new Model<>("TEST");
//		Label label = new Label("xxx", labelModel);
//		label.setOutputMarkupId(true);
//		add(label);
//
//		add(new Label("demo", "dialog2"));
//		add(new BookmarkablePageLink<>("linkid", FormPage.class));

//	add(new Link("linkid") {

//		@Override
//		public void onClick() {
//			HomePage.this.setResponsePage(TestPage.class);
//		}
//	})
//		add(new BookmarkablePageLink<>("linkid", TestPage.class));
//
//		i = 10;
//		add(new AjaxLink<String>("linkid") {
//			@Override
//			public void onClick(AjaxRequestTarget target) {
//				i++;
//				labelModel.setObject("klik " + i);
//				target.add(label);
//			}
//		});
//	}
//}

//public class HomePage extends WebPage {
//	private static final long serialVersionUID = 1L;
//
//
//	public HomePage(final PageParameters parameters) {
//		super(parameters);
//
//		add(new Label("version", "ddd: " + getApplication().getFrameworkSettings().getVersion()));
//
//		Model<String> labelModel = new Model<>("TEST");
//		Label label = new Label("xxx", labelModel);
//		label.setOutputMarkupId(true);
//		add(label);
//	}
//}}
//}}