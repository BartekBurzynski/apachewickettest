package com.mycompany;

import java.io.Serializable;

public class Person implements Serializable {
    private String name;
    private String lastName;
    private String city;
    private String position;

    public Person(String name, String lastName, String city, String position) {
        this.name = name;
        this.lastName = lastName;
        this.city = city;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
