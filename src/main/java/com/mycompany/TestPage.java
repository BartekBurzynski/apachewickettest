package com.mycompany;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class TestPage extends WebPage {
    private static final long serialVersionUID = 1L;

    public TestPage(final PageParameters parameters) {
        super(parameters);
        add(new Label("test2", "nextTest"));

    }
}
