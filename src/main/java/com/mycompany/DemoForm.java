//package com.mycompany;
//
//import org.apache.wicket.markup.html.WebPage;
//import org.apache.wicket.markup.html.basic.Label;
//import org.apache.wicket.markup.html.form.*;
//import org.apache.wicket.markup.html.list.ListItem;
//import org.apache.wicket.markup.html.list.ListView;
//import org.apache.wicket.model.CompoundPropertyModel;
//import org.apache.wicket.model.Model;
//import org.apache.wicket.model.util.ListModel;
//import org.apache.wicket.request.mapper.parameter.PageParameters;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class PersonListDetails extends WebPage {
//    private Form<Void> form;
//    private DropDownChoice<Person> personsList;
//
//    public PersonListDetails(){
//        Model<Person> listModel = new Model<Person>();
//        ChoiceRenderer<Person> personRender = new ChoiceRenderer<Person>("fullName");
//
//        personsList = new DropDownChoice<Person>("persons", listModel, loadPersons(), personRender);
//        personsList.add(new FormComponentUpdatingBehavior());
//
//        add(personsList);
//
//        form = new Form<>("form", new CompoundPropertyModel<Person>(listModel));
//        form.add(new TextField("name"));
//        form.add(new TextField("surname"));
//        form.add(new TextField("address"));
//        form.add(new TextField("email"));
//
//        add(form);
//    }
//
//    private List<Person>  loadPersons() {
//        personsList.add(DropDownChoice);
//    }
//
//    //loadPersons()
//    //...
//}
